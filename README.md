Use keybindings of your linux desktop environment to control several features of your computer very quickly. Such as:

* Audio output choice (HDMI / internal)
* Volume
* Player control (play/pause, next/prev, etc.)
* Video output selection / toggling / resolution
* Panel position / reloading
* Starting several applications

I like these simple bindings because I use different keyboards in different machines,
and can only assume they have little in common.
With these simple bindings, I can use all the quick operations I wanna do, anywhere, always in the same way.
