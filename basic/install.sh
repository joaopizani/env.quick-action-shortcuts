#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SYSD_USER_BINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSD_USER_BINARIES:-"${HOME}/.local/bin"}"


mkdir -p "${BINDIR}"
ln -snf "${DIR}/quick-action-dispatcher.sh" "${BINDIR}/"

"${DIR}/keybindings-install/bindings-select.sh"

