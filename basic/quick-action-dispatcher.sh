#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


if [ $# -gt 0 ]; then
    SCRIPTNAME="${1}"

    shift 1
    bash "${DIR}/individual-action-scripts/${SCRIPTNAME%=}.sh" "$@"
fi

