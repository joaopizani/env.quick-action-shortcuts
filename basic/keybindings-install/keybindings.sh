MODKEY="<Primary><Mod4>"

QUICKACT_BINDINGS["display_primary-switch"]="${MODKEY}grave"
QUICKACT_BINDINGS["display_xmlcfg-on-highest="]="${MODKEY}1"
QUICKACT_BINDINGS["display_xmlcfg-on2="]="${MODKEY}q"
QUICKACT_BINDINGS["display_xmlcfg-on1="]="${MODKEY}a"
QUICKACT_BINDINGS["display_xmlcfg-off="]="${MODKEY}z"

QUICKACT_BINDINGS["audio_vol-up-except-100"]="${MODKEY}9"
QUICKACT_BINDINGS["audio_vol-down"]="${MODKEY}8"
QUICKACT_BINDINGS["audio_sink-conf0="]="${MODKEY}comma"
QUICKACT_BINDINGS["audio_sink-conf1="]="${MODKEY}k"
QUICKACT_BINDINGS["audio_sink-conf2="]="${MODKEY}i"

QUICKACT_BINDINGS["brightness_native-up"]="${MODKEY}4"
QUICKACT_BINDINGS["brightness_native-down"]="${MODKEY}3"
QUICKACT_BINDINGS["brightness_ext-3"]="${MODKEY}2"
QUICKACT_BINDINGS["brightness_ext-2"]="${MODKEY}w"
QUICKACT_BINDINGS["brightness_ext-1"]="${MODKEY}s"
QUICKACT_BINDINGS["brightness_ext-0"]="${MODKEY}x"

QUICKACT_BINDINGS["launch_retroarch="]="${MODKEY}e"
QUICKACT_BINDINGS["launch_youtube="]="${MODKEY}d"
QUICKACT_BINDINGS["launch_radio="]="${MODKEY}c"

QUICKACT_BINDINGS["efibootmgr_next-Windows="]="${MODKEY}5"
QUICKACT_BINDINGS["efibootmgr_next-clear="]="${MODKEY}t"

QUICKACT_BINDINGS["player-ctl_stop"]="${MODKEY}7"
QUICKACT_BINDINGS["player-ctl_playpause"]="${MODKEY}u"
QUICKACT_BINDINGS["player-ctl_prev"]="${MODKEY}j"
QUICKACT_BINDINGS["player-ctl_next"]="${MODKEY}m"
QUICKACT_BINDINGS["player-ctl_singleoff"]="${MODKEY}6"
QUICKACT_BINDINGS["player-ctl_singleon"]="${MODKEY}y"
QUICKACT_BINDINGS["player-ctl_loopnone"]="${MODKEY}h"
QUICKACT_BINDINGS["player-ctl_looplist"]="${MODKEY}n"

QUICKACT_BINDINGS["nm_networking-toggle="]="${MODKEY}r"
QUICKACT_BINDINGS["mousekeys-toggle"]="${MODKEY}f"
QUICKACT_BINDINGS["notification-dnd-toggle="]="${MODKEY}v"

QUICKACT_BINDINGS["touchpad-enable"]="${MODKEY}g"
QUICKACT_BINDINGS["touchpad-disable"]="${MODKEY}b"

QUICKACT_BINDINGS["tlp_ctrl-setcharge"]="${MODKEY}o"
QUICKACT_BINDINGS["tlp_ctrl-fullcharge"]="${MODKEY}l"
QUICKACT_BINDINGS["display_kill-matepanel"]="${MODKEY}p"

QUICKACT_BINDINGS["mediakeys_ltoggle6="]="Tools"
QUICKACT_BINDINGS["mediakeys_ltoggle8="]="Search"
QUICKACT_BINDINGS["mediakeys_ltoggle9="]="Mail"
QUICKACT_BINDINGS["mediakeys_ltoggle3="]="Explorer"

