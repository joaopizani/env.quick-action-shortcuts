SCHEMA_STR_=""; for p in ${SCHEMA_ARR[@]}; do SCHEMA_STR_+="${p}."; done
PATH_STR_="/"; for p in ${PATH_ARR[@]}; do PATH_STR_+="${p}/"; done

SCHEMA_STR="${SCHEMA_STR_}${SS}"
PATH_STR="${PATH_STR_}${SS}s"


declare -A QUICKACT_BINDINGS
source "${DIRUP}/keybindings.sh"


GRPNAME="qas"
PREFIX="yyy-${GRPNAME}"

if [[ "${1}" == "reset" ]]; then

    for actname in "${!QUICKACT_BINDINGS[@]}"; do
        gsettings reset-recursively "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${actname}/"
    done

else

    for actname in "${!QUICKACT_BINDINGS[@]}"; do
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${actname}/" "name"    "'${GRPNAME}-${actname%=}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${actname}/" "action"  "'quick-action-dispatcher.sh ${actname%=}'"
        gsettings set "${SCHEMA_STR}:${PATH_STR}/${PREFIX}-${actname}/" "binding" "'${QUICKACT_BINDINGS["${actname}"]}'"
    done

fi

