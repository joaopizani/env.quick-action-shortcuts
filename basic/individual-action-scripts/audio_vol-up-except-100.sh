#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

VOLINCDEFAULT="5"
VOLINC="${1:-"${VOLINCDEFAULT}"}"

DEF_SINK_NAME="$(pactl info | sed -En 's/Default Sink: (.*)/\1/p')"
CUR_VOL=$(pactl list sinks | perl -000ne "if(/$DEF_SINK_NAME/){/(Volume:.*)/; print \"\$1\n\"}" | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')

if [ $CUR_VOL -lt 100 ]; then pactl set-sink-volume @DEFAULT_SINK@ "+${VOLINC}%"; fi

