CHOICE_CLEAR="clear"
CHOICE_DEFAULT="${CHOICE_CLEAR}"
CHOICE="${1:-"${CHOICE_DEFAULT}"}"

ALLENTRIES="$(efibootmgr)"
CHOSENENTRYID="$(echo "${ALLENTRIES}" | grep "${CHOICE}")"


if [[ "${CHOICE}" == "${CHOICE_CLEAR}" ]]; then

    efibootmgr --delete-bootnext

else

    if [[ -z "${CHOSENENTRYID}" ]]; then
        CHOSENENTRYID="Could not find entry (not changed)"
    else
        ENTRYIDX="$(echo "${CHOSENENTRYID}" | cut -c 5-8)"
        efibootmgr --bootnext "${ENTRYIDX}"
    fi

fi

