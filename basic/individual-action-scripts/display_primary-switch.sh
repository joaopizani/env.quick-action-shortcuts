#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

xrandr --output "$(xrandr --listactivemonitors | tail -n 1 | awk '{print $4}')" --primary
pkill mate-panel

