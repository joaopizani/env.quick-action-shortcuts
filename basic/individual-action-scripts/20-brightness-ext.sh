#!/usr/bin/env bash

CONFIG_DIR="${XDG_CONFIG_HOME:-"${HOME}/.config"}/quick-action_brightness"
CONFIG_FILE="${CONFIG_DIR}/buses-levels"

export BRIGHTVCP="10"


declare -a BUSES LVLS_0 LVLS_1 LVLS_2 LVLS_3  # all aligned (zipped) to same length

mkdir -p "${CONFIG_DIR}"
if [ ! -s "${CONFIG_FILE}" ]; then
  while read BID; do BUSES+=( "${BID}" )
  done < <(ddcutil detect --terse | grep I2C | cut -d '/' -f 3 | cut -d '-' -f 2)

  # Put all detected buses with levels of 0%, 25%, 50% and 100%
  for BUS in "${BUSES[@]}"; do
    LVLMAX="$(ddcutil getvcp --terse "${BRIGHTVCP}" --bus "${BUS}" | cut -d ' ' -f 5)"
    LVLNORM="$(echo "scale=2; ${LVLMAX} / 100" | bc)"
    # magical awk incantations needed to round a number from 2 decimals to none
    LVL025="$(echo "scale=2; 25 * ${LVLNORM}" | bc | awk '{printf("%d\n",$1 + 0.5)}')"
    LVL050="$(echo "scale=2; 50 * ${LVLNORM}" | bc | awk '{printf("%d\n",$1 + 0.5)}')"
    echo "${BUS} 0 ${LVL025} ${LVL050} ${LVLMAX}" >>"${CONFIG_FILE}"
  done
fi

while read BID L0 L1 L2 L3; do
  BUSES+=( "${BID}" )
  LVLS_0+=( "${L0}" )
  LVLS_1+=( "${L1}" )
  LVLS_2+=( "${L2}" )
  LVLS_3+=( "$(echo "${L3}" | cut -d ' ' -f 1)" )  # select only first word
done <"${CONFIG_FILE}"

LVLSVARNAME="LVLS_${1}[@]"
LVLS=( "${!LVLSVARNAME}" )
echo "Chosen levels: ${LVLS[@]}"


__callDDC() {
  for i in "${!BUSES[@]}"; do echo "${BUSES[$i]} ${LVLS[$i]}"; done |
    xargs -P0 -n2 sh -c "ddcutil setvcp ${BRIGHTVCP} \$1 --bus \$0"
}

__callDDC
__callDDC
