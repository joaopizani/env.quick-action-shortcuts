#!/usr/bin/env bash
NAM="${BASH_SOURCE[0]}"
FIL="$(readlink -f "${NAM}")"
DIR="$(dirname "${FIL}")"

LVL_IDX="${NAM:(-4):-3}"  # -3 removes '.sh', from -4 means we only get one char ('0', '1', etc.)

source "${DIR}/20-brightness-ext.sh" "${LVL_IDX}"

