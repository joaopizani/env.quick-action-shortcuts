#!/usr/bin/env bash

(
  env APPIMAGELAUNCHER_DISABLE=1 $(echo "$(systemd-path user-binaries)"/*RetroArch* | head -n 1)

) </dev/null &>/dev/null  &  disown

