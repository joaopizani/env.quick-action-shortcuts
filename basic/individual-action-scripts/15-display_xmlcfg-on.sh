#!/usr/bin/env bash

MONITOR_XML="${XDG_CONFIG_HOME:-"${HOME}/.config"}/monitors.xml"


CONFIG_AMOUNT="$(xmllint --xpath "count(//monitors/configuration)" "${MONITOR_XML}" 2>/dev/null)"

# default is highest (latest)
CONFIG_PASSED="${1}"
CONFIG_CHOSEN=${CONFIG_AMOUNT}
if [[ ${CONFIG_PASSED} -ge 1 && ${CONFIG_PASSED} -le ${CONFIG_AMOUNT} ]]; then
  CONFIG_CHOSEN=${CONFIG_PASSED}
else
  [[ -n "${CONFIG_PASSED}" ]]  &&  echo "Invalid configuration index passed as parameter, using default (highest): ${CONFIG_CHOSEN}"
fi


OUTPUTS_AMOUNT="$(xmllint --xpath "count(//monitors/configuration[${CONFIG_CHOSEN}]/output)" "${MONITOR_XML}" 2>/dev/null)"
for (( i=1; i<=$OUTPUTS_AMOUNT; i++)); do
  NAME="$(xmllint --xpath "string(//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/@name)" "${MONITOR_XML}" 2>/dev/null)"
  POS_X="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/x/text()" "${MONITOR_XML}" 2>/dev/null)"
  POS_Y="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/y/text()" "${MONITOR_XML}" 2>/dev/null)"
  ROTATE="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/rotation/text()" "${MONITOR_XML}" 2>/dev/null)"
  WIDTH="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/width/text()" "${MONITOR_XML}" 2>/dev/null)"
  HEIGHT="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/height/text()" "${MONITOR_XML}" 2>/dev/null)"
  RATE="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/rate/text()" "${MONITOR_XML}" 2>/dev/null)"
  PRIMARY="$(xmllint --xpath "//monitors/configuration[${CONFIG_CHOSEN}]/output[${i}]/primary/text()" "${MONITOR_XML}" 2>/dev/null)"

  PARAM_ARR=( "${PARAM_ARR[@]}" "--output" "$NAME" )

  # if position is defined, add position and orientation to command line parameters, otherwise turn monitor off
  if [ -n "$POS_X" ]; then
    PARAM_ARR=( "${PARAM_ARR[@]}" \
                "--mode" "${WIDTH}"x"${HEIGHT}" \
                "--pos" "${POS_X}x${POS_Y}" \
                "--fbmm" "${WIDTH}x${HEIGHT}" \
                "--rate" "$RATE" \
                "--rotate" "$ROTATE")

    # if monitor is defined as primary, adds it to command line parameters
    [ "$PRIMARY" = "yes" ] && PARAM_ARR=("${PARAM_ARR[@]}" "--primary")

  else
    PARAM_ARR=( "${PARAM_ARR[@]}" "--off" )
  fi

done


# turn on all monitors with obtained positions and sizes
xrandr ${PARAM_ARR[@]}
