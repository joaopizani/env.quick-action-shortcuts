#!/usr/bin/env bash

MONITOR_XML="${XDG_CONFIG_HOME:-"${HOME}/.config"}/monitors.xml"

# Default monitor considered internal is the first monitor of the first configuration
DEFAULT_INTERNAL_PRODUCT="$(xmllint --xpath "//monitors/configuration[1]/output[1]/product/text()" "${MONITOR_XML}" 2>/dev/null)"
DEFAULT_INTERNAL_SERIAL="$(xmllint --xpath "//monitors/configuration[1]/output[1]/serial/text()" "${MONITOR_XML}" 2>/dev/null)"

CUSTOM_INTERNAL_FILE="${XDG_CONFIG_HOME:-"${HOME}/.config"}/display-xmlcfg-on-off/product-serial_internal"
CUSTOM_INTERNAL_PRODUCT="$(cut -f 1 <"${CUSTOM_INTERNAL_FILE}" 2>/dev/null)"
CUSTOM_INTERNAL_SERIAL="$(cut -f 2 <"${CUSTOM_INTERNAL_FILE}" 2>/dev/null)"

INTERNAL_PRODUCT="${1:-"${CUSTOM_INTERNAL_PRODUCT:-"${DEFAULT_INTERNAL_PRODUCT}"}"}"
INTERNAL_SERIAL="${2:-"${CUSTOM_INTERNAL_SERIAL:-"${DEFAULT_INTERNAL_SERIAL}"}"}"


CONFIG_AMOUNT="$(xmllint --xpath "count(//monitors/configuration)" "${MONITOR_XML}" 2>/dev/null)"
XRANDR_PARAMS=()
for (( j=1; j<=${CONFIG_AMOUNT}; j++)); do
  N_OUTS=$(xmllint --xpath "count(//monitors/configuration[${j}]/output)" "${MONITOR_XML}" 2>/dev/null)

  for (( k=1; k<=$N_OUTS; k++)); do
    PRODUCT=$(xmllint --xpath "//monitors/configuration[${j}]/output[${k}]/product/text()" "${MONITOR_XML}" 2>/dev/null)
    SERIAL=$(xmllint --xpath "//monitors/configuration[${j}]/output[${k}]/serial/text()" "${MONITOR_XML}" 2>/dev/null)

    if [[ ! ( "${PRODUCT}" == "${INTERNAL_PRODUCT}" && "${SERIAL}" == "${INTERNAL_SERIAL}" ) ]]; then
      CONFIG_CHOSEN="${j}"
      OUT_NAME=$(xmllint --xpath "string(//monitors/configuration[${CONFIG_CHOSEN}]/output[${k}]/@name)" "${MONITOR_XML}" 2>/dev/null)
      XRANDR_PARAMS+=("--output" "${OUT_NAME}" "--off")
    fi
  done

done

xrandr "${XRANDR_PARAMS[@]}"
