#!/usr/bin/env bash
#DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SETTING_1_PATH="org.mate.NotificationDaemon"
SETTING_1_KEY="do-not-disturb"

SETTING_2_PATH="org.ayatana.indicator.notifications"
SETTING_2_KEY="do-not-disturb"


NOTIF_DND_NEW_VAL="false"
NOTIF_DND_CUR_VAL="$(gsettings get "${SETTING_1_PATH}" "${SETTING_1_KEY}")"

[[ "${NOTIF_DND_CUR_VAL}" == "false" ]] && NOTIF_DND_NEW_VAL="true"


gsettings set "${SETTING_1_PATH}" "${SETTING_1_KEY}" "${NOTIF_DND_NEW_VAL}"
gsettings set "${SETTING_2_PATH}" "${SETTING_2_KEY}" "${NOTIF_DND_NEW_VAL}"

