#!/usr/bin/env bash
SRCPATH="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "${SRCPATH}")"

CHOSEN_DEFAULT="setcharge"
CHOSEN="${1:-"${CHOSEN_DEFAULT}"}"

NOTIFICATION_TIME=1300


SYSDUSERBINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSDUSERBINARIES:-"${HOME}/.local/bin"}"

CHOICEFILE_PREFIX="tlp_ctrl-"
SCRIPT_BASENAME="$(basename "${SRCPATH}" ".sh")"
SUDOSCRIPT_BASENAME="${SCRIPT_BASENAME}_sudo"

SUDOERSD_FILE="/etc/sudoers.d/${SCRIPT_BASENAME}"
if [ ! -f "${SUDOERSD_FILE}" ]; then
    SUDO_ASKPASS="/usr/libexec/seahorse/ssh-askpass" sudo --askpass -v &>/dev/null

    mkdir -p "/etc/sudoers.d"
    sudo touch "${SUDOERSD_FILE}"
    sudo chown root:root "${SUDOERSD_FILE}"
    sudo chmod 440 "${SUDOERSD_FILE}"

    shopt -q -s nullglob;  CHOICEFILES=( "${DIR}/${CHOICEFILE_PREFIX}"* );  shopt -q -u nullglob

    for cf in "${CHOICEFILES[@]}"; do
        CHOICE_BASENAME="$(basename "${cf}" ".sh")"
        CHOICE_PURE="${CHOICE_BASENAME##"${CHOICEFILE_PREFIX}"}"
        LINE_="${USER} ALL = (root) NOPASSWD: ${BINDIR}/quick-action-dispatcher.sh ${SUDOSCRIPT_BASENAME} ${CHOICE_PURE}"
        echo "${LINE_}" | sudo tee -a "${SUDOERSD_FILE}" &>/dev/null
    done
fi


sudo "${BINDIR}/quick-action-dispatcher.sh" "${SUDOSCRIPT_BASENAME}" "${CHOSEN}"


notify-send --expire-time=${NOTIFICATION_TIME} "TLP Ctrl" "${CHOSEN}"
