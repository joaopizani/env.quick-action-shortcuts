#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


nmcli networking connectivity | grep -q none \
  && nmcli networking on \
  || nmcli networking off

