#!/usr/bin/env bash

BRIGHTPERCDELTADEFAULT="10"
BRIGHTPERCDELTA="${1:-"${BRIGHTPERCDELTADEFAULT}"}"

BRIGHTMAX="$(mate-power-backlight-helper --get-max-brightness)"

BRIGHTCUR="$(mate-power-backlight-helper --get-brightness)"
BRIGHTDELTA="$(( ${BRIGHTPERCDELTA} * ${BRIGHTMAX} / 100  ))"

BRIGHTNEWUP="$(( ${BRIGHTCUR} + ${BRIGHTDELTA} ))"
BRIGHTNEWDOWN="$(( ${BRIGHTCUR} - ${BRIGHTDELTA} ))"

# Ceiling and floor
[[ ${BRIGHTNEWUP}   -gt ${BRIGHTMAX} ]]  &&  BRIGHTNEWUP=${BRIGHTMAX}
[[ ${BRIGHTNEWDOWN} -lt 0            ]]  &&  BRIGHTNEWDOWN=0


if [[ ${BRIGHTPERCDELTA} -eq 100 ]]; then  # The delta percent value of 100 has a state save/restore behaviour

  STATEFILE="${XDG_RUNTIME_DIR:-"/run/user/$(id -u)"}/qas_20-brightness-native_state"

  if [[ -s "${STATEFILE}" ]]; then
    { FROMSTATE="$(<"${STATEFILE}")"; } 2>/dev/null
    BRIGHTNEWUP="${FROMSTATE}"
    BRIGHTNEWDOWN="${FROMSTATE}"
    rm -f "${STATEFILE}"
  else
    mkdir -p "$(dirname "${STATEFILE}")"
    echo "${BRIGHTCUR}" > "${STATEFILE}"
  fi

fi


export BRIGHTNEWUP BRIGHTNEWDOWN

