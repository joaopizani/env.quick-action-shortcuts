#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


CONFIDX_DEFAULT=0
CONFIDX="${1:-"${CONFIDX_DEFAULT}"}"

PARENTCONFDIR="${XDG_DATA_HOME:-"${HOME}/.local/share"}"
CONFDIR="${PARENTCONFDIR}/audio_sink-conf"
mkdir -p "${CONFDIR}"

shopt -q -s nullglob
CONFFILES=( "${CONFDIR}/"* )
shopt -q -u nullglob

CONFFILE="$(readlink -f "${CONFFILES[${CONFIDX}]}" 2>/dev/null)"
[[ -z "${CONFFILE}" ]]  &&  exit 42


CHOSENCARD="$(awk 'NR==1' "${CONFFILE}")"
CHOSENPROF="$(awk 'NR==2' "${CONFFILE}")"
CHOSENSINK="$(awk 'NR==3' "${CONFFILE}")"

pactl set-card-profile "${CHOSENCARD}" "${CHOSENPROF}"
pactl set-default-sink "${CHOSENSINK}"

