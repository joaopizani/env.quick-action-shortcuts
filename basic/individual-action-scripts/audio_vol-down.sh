#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

VOLDECDEFAULT="5"
VOLDEC="${1:-"${VOLDECDEFAULT}"}"

pactl set-sink-volume @DEFAULT_SINK@ "-${VOLDEC}%"

