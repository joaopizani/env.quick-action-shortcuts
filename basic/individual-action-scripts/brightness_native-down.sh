#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


source "${DIR}/20-brightness-native.sh" ${1}

pkexec mate-power-backlight-helper --set-brightness "${BRIGHTNEWDOWN}"

