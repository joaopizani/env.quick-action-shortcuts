#!/usr/bin/env bash
#DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SETTING_MOUSEKEYS_PATH="org.mate.accessibility-keyboard"
SETTING_MOUSEKEYS_KEY="mousekeys-enable"


MOUSEKEYS_NEW_VAL="false"
MOUSEKEYS_CUR_VAL="$(gsettings get "${SETTING_MOUSEKEYS_PATH}" "${SETTING_MOUSEKEYS_KEY}")"

[[ "${MOUSEKEYS_CUR_VAL}" == "false" ]] && MOUSEKEYS_NEW_VAL="true"


gsettings set "${SETTING_MOUSEKEYS_PATH}" "${SETTING_MOUSEKEYS_KEY}" "${MOUSEKEYS_NEW_VAL}"

