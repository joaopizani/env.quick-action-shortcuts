#!/usr/bin/env python3
import re
from itertools import groupby
from pathlib import Path

with open(Path(__file__).resolve().parent.parent / 'basic' / 'keybindings-install' / 'keybindings.sh') as kbf:
    acts = [ m.group('action')  for m in re.finditer('\["(?P<action>.*)"\]=', kbf.read()) ]

grpUnderscorePref =  lambda l: [ list(grp)  for j,grp in groupby(l, lambda x: x.partition('_')[0]) ]


numBoxesColumn = 16
grpSpacing=8
innerSpacing = 1

import gi; gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from subprocess import Popen

def actMaybeClose(_, s):
    Popen(["quick-action-dispatcher.sh",s])
    if s[-1] == '=': Gtk.main_quit()

leftButs = [ [ (a, Gtk.Button(a)) for a in g ]  for g in grpUnderscorePref(acts[0:numBoxesColumn]) ]
riteButs = [ [ (a, Gtk.Button(a)) for a in g ]  for g in grpUnderscorePref(acts[numBoxesColumn:(numBoxesColumn*2)])  ]
[ t.connect("clicked", actMaybeClose, l)  for (l,t) in sum(leftButs,[]) + sum(riteButs,[]) ]

leftInBoxes = [ (ls, Gtk.Box(spacing=innerSpacing, orientation=Gtk.Orientation.VERTICAL)) for ls in leftButs ]
riteInBoxes = [ (ls, Gtk.Box(spacing=innerSpacing, orientation=Gtk.Orientation.VERTICAL)) for ls in riteButs ]
[ [ b.pack_start(t, True, True, 0) for (_, t) in ls ]  for (ls, b) in leftInBoxes + riteInBoxes ]

leftBox = Gtk.Box(spacing=grpSpacing, orientation=Gtk.Orientation.VERTICAL)
riteBox = Gtk.Box(spacing=grpSpacing, orientation=Gtk.Orientation.VERTICAL)
[ leftBox.pack_start(b, True, True, 0)  for (_, b) in leftInBoxes ]
[ riteBox.pack_start(b, True, True, 0)  for (_, b) in riteInBoxes ]

mainBox = Gtk.Box(spacing=20, orientation=Gtk.Orientation.HORIZONTAL)
[ mainBox.pack_start(b, True, True, 0)  for b in [leftBox, riteBox] ]


w = Gtk.Window(title="quick-actions-gui-gtk", modal=True, resizable=False, role="MAIN" , default_width=580, default_height=715)
w.connect("destroy", Gtk.main_quit)
w.add(mainBox)
w.show_all()
Gtk.main()
