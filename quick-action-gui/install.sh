#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"

SYSD_USER_BINARIES="$(systemd-path user-binaries)"
BINDIR="${SYSD_USER_BINARIES:-"${HOME}/.local/bin"}"

DESKTOPAPPSDIR="${XDG_DATA_HOME:-"${HOME}/.local/share"}/applications"


mkdir -p "${BINDIR}"
ln -sfn "${DIR}/quick-action-gui.py" "${BINDIR}/"

mkdir -p "${DESKTOPAPPSDIR}"
cp "${DIR}/desktop/quick-action-gui.desktop" "${DESKTOPAPPSDIR}"
xdg-icon-resource install --novendor --size 48 "${DIR}/desktop/icons/hicolor/48x48/apps/quick-action-gui.png"

